package com.basic.mapper;

import com.basic.models.dtos.Contact;
import com.basic.models.dtos.ContactInformation;

public class MapperContact {
	
	public static ContactInformation mapperContactToContactDto(Contact contact) {
		ContactInformation contactDto = new ContactInformation();
		contactDto.setId(contact.getId());
		contactDto.setName(contact.getName());
		contactDto.setUsername(contact.getUsername());
		contactDto.setEmail(contact.getEmail());
		contactDto.setPhone(contact.getPhone());
		contactDto.setWebsite(contact.getWebsite());
		
		contactDto.setStreet(contact.getAddress()!=null?contact.getAddress().getStreet():null);
		contactDto.setSuite(contact.getAddress()!=null?contact.getAddress().getSuite():null);
		contactDto.setCity(contact.getAddress()!=null?contact.getAddress().getCity():null);
		contactDto.setZipcode(contact.getAddress()!=null?contact.getAddress().getZipcode():null);
		
		contactDto.setLat((contact.getAddress()!=null&&contact.getAddress().getGeo()!=null)?contact.getAddress().getGeo().getLat():null);
		contactDto.setLng((contact.getAddress()!=null&&contact.getAddress().getGeo()!=null)?contact.getAddress().getGeo().getLng():null);
		
		contactDto.setCompanyName(contact.getCompany()!=null?contact.getCompany().getName():null);
		contactDto.setCatchPhrase(contact.getCompany()!=null?contact.getCompany().getName():null);
		contactDto.setBs(contact.getCompany()!=null?contact.getCompany().getBs():null);
		
		return contactDto;
	}
	
	
}
