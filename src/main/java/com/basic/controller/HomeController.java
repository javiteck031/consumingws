package com.basic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.basic.mapper.MapperContact;
import com.basic.models.daos.ICalculationDao;
import com.basic.models.daos.IContactDao;
import com.basic.models.dtos.Calculation;
import com.basic.models.dtos.Contact;
import com.basic.models.dtos.ContactInformation;
import com.basic.soap.calculator.Calculator;
import com.basic.soap.calculator.CalculatorSoap;

@RestController
public class HomeController {
	
	private static String URI = "https://jsonplaceholder.typicode.com/users";

	@Autowired
	IContactDao personDao;
	
	@Autowired
	ICalculationDao calculationDao;
	
	@RequestMapping(value="/rest", method=RequestMethod.GET)
	public List<ContactInformation> consumingRest() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<List<Contact>> responseEntity = restTemplate.exchange(
				URI, 
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<List<Contact>>() {});
		List<Contact> contacts = responseEntity.getBody();
		for(Contact contact: contacts) {
			personDao.save(MapperContact.mapperContactToContactDto(contact));
		}
		
		return personDao.findAll();
	}

	
	@RequestMapping(value="/soap", method=RequestMethod.GET)
	public List<Calculation> consumingSoap(@RequestParam(required=true) Integer a, @RequestParam(required=true) Integer b) {
		CalculatorSoap cs = new Calculator().getCalculatorSoap();
		calculationDao.save(new Calculation("ADD", a, b,cs.add(a,b)));
		calculationDao.save(new Calculation("SUBSTRACT", a, b,cs.subtract(a,b)));
		calculationDao.save(new Calculation("MULTIPLY", a, b,cs.multiply(a,b)));
		calculationDao.save(new Calculation("DIVIDE", a, b,cs.divide(a,b)));
		return calculationDao.findAll();
	}

}
