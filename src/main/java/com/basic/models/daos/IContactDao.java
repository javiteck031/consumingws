package com.basic.models.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.basic.models.dtos.ContactInformation;

public interface IContactDao extends JpaRepository<ContactInformation,Long> {
}
