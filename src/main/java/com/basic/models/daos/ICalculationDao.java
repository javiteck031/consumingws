package com.basic.models.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.basic.models.dtos.Calculation;

public interface ICalculationDao extends JpaRepository<Calculation,Long> {

}
