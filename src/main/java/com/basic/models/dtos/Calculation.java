package com.basic.models.dtos;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Calculation implements Serializable{
	
	private static final long serialVersionUID = 4894729030347835498L;
	
	@Id
	@GeneratedValue
	private Integer id;
	private String type;
	private Integer valueA;
	private Integer valueB;
	private Integer result;
	
	public Calculation() {
		super();
	}

	public Calculation(String type, Integer valueA, Integer valueB, Integer result) {
		super();
		this.type = type;
		this.valueA = valueA;
		this.valueB = valueB;
		this.result = result;
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Integer getValueA() {
		return valueA;
	}


	public void setValueA(Integer valueA) {
		this.valueA = valueA;
	}


	public Integer getValueB() {
		return valueB;
	}


	public void setValueB(Integer valueB) {
		this.valueB = valueB;
	}


	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "OperationDto [id=" + id + ", type=" + type + ", valueA=" + valueA + ", valueB=" + valueB + ", result="
				+ result + "]";
	}
	
}
